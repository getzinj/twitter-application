﻿using System.Collections.Generic;
using System.IO;

namespace com.jeffreygetzin.twitterExercise
{
    /// <summary>
    /// A class capable of displaying tweets.
    /// </summary>
    public class TweetRenderer : ITweetRenderer
    {
        private readonly TextWriter outputStream;

        public TweetRenderer(TextWriter outputStream)
        {
            this.outputStream = outputStream;
        }

        /// <summary>
        /// Display all the specified tweets (without showing the usernames of the persons who
        /// tweeted them).
        /// </summary>
        public void displayTweets(IEnumerable<TwitterDataSet.TweetsDataTableRow> tweets)
        {
            outputStream.WriteLine();
            foreach (TwitterDataSet.TweetsDataTableRow tweet in tweets)
            {
                outputStream.WriteLine(tweet.Message + " (" + tweet.Timestamp.Delta() + ')');
            }
            outputStream.WriteLine();
        }

        /// <summary>
        /// Display all the specified tweets, showing the usernames of the persons who tweeted 
        /// them.
        /// </summary>
        public void displayTweetsWithUsernames(IEnumerable<TwitterDataSet.TweetsDataTableRow> tweets)
        {
            outputStream.WriteLine();
            foreach (TwitterDataSet.TweetsDataTableRow tweet in tweets)
            {
                outputStream.WriteLine(tweet.GetUsername() + " - " + tweet.Message + " (" + tweet.Timestamp.Delta() + ')');
            }
            outputStream.WriteLine();
        }
    }
}
