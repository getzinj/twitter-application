﻿namespace com.jeffreygetzin.twitterExercise
{
    /// <summary>
    /// Represents the permitted set of Twitter-like commands.
    /// </summary>
    public enum TwitterCommand
    {
        Error,
        Posting,
        Reading,
        Following,
        Wall
    }
}
