﻿namespace com.jeffreygetzin.twitterExercise
{
    /// <summary>
    /// Represents a device capable of displaying a prompt string and parsing a user-inputted command.
    /// </summary>
    interface IPrompt
    {
        /// <summary>
        /// Prompt the user for a command-line and parse the results.
        /// </summary>
        void PromptForUserInput(out TwitterCommand command, out string username1, out string username2, out string message);
    }
}
