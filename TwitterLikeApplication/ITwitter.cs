﻿namespace com.jeffreygetzin.twitterExercise
{
    /// <summary>
    /// Represents a Twitter-like service, which allows posting and reading tweets.
    /// </summary>
    internal interface ITwitter
    {
        /// <summary>
        /// The first user begins following the second.
        /// </summary>
        void Following(string username1, string username2);

        /// <summary>
        /// The specified user posts a message.
        /// </summary>
        void Posting(string username, string message);

        /// <summary>
        /// The specified user reads his own messages.
        /// </summary>
        void Reading(string username);

        /// <summary>
        /// Reads the wall of the specified user. This wall includes all posts he himself made,
        /// plus any posts users he follows have posted since he started following them.
        /// </summary>
        void Wall(string username);
    }
}