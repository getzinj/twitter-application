﻿using System.IO;
using TwitterLikeApplication;

namespace com.jeffreygetzin.twitterExercise
{
    /// <summary>
    /// A class capable of prompting a user for a command, reading in that command, and parsing the result.
    /// </summary>
    public class Prompt: IPrompt
    {
        private const string PROMPT_TEXT = "> ";
        private static readonly TwitterCommandParser PARSER = new TwitterCommandParser();

        private readonly TextWriter outputStream;
        private readonly TextReader inputStream;

        public Prompt(TextWriter outputStream, TextReader inputStream)
        {
            this.outputStream = outputStream;
            this.inputStream = inputStream;
        }

        /// <summary>
        /// Prompt the user for a command-line and parse the results.
        /// </summary>
        public void PromptForUserInput(out TwitterCommand command, out string username1, out string username2, out string message)
        {
            outputStream.Write(PROMPT_TEXT);
            string userInput = inputStream.ReadLine();

            PARSER.parse(userInput, out command, out username1, out username2, out message);
        }
    }
}
