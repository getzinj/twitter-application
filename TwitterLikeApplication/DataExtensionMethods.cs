﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace com.jeffreygetzin.twitterExercise
{
    /// <summary>
    /// A collection of extension methods to abstract away the manipulation of the generated <see cref="TwitterDataSet"/>.
    /// </summary>
    static class DataExtensionMethods
    {
        /// <summary>
        /// Returns all tweets for the specified user.
        /// </summary>
        public static IEnumerable<TwitterDataSet.TweetsDataTableRow> ReadTweets(this TwitterDataSet data, string username)
        {
            return getTweetsForUser(data, data.GetUserIdForUsername(username));
        }

        /// <summary>
        /// Returns the tweets displayed for a user's wall.
        /// </summary>
        /// 
        /// <remarks>
        /// The tweets displayed will be all tweets this user himself made since the beginning of time 
        /// plus all tweets all followed users have made since this user started following them.
        /// </remarks>
        public static IEnumerable<TwitterDataSet.TweetsDataTableRow> GetWallTweets(this TwitterDataSet data, string username)
        {
            IEnumerable<TwitterDataSet.TweetsDataTableRow> myTweets = getAllTweetsForUsername(data, username);

            return combineTweets(myTweets, getFollowedTweets(data, username));
        }

        public static String GetUsername(this TwitterDataSet.TweetsDataTableRow tweet)
        {
            return ((TwitterDataSet)tweet.Table.DataSet).GetUsernameForUserId(tweet.UserId);
        }


        /// <summary>
        /// Looks up the username for the user with the specified userId.
        /// </summary>
        /// 
        /// <remarks>
        /// The method assumes that the user actually exists. Do not call this method without
        /// verifying that.
        /// </remarks>
        public static string GetUsernameForUserId(this TwitterDataSet data, long userId)
        {
            return
                (from x in data.UserDataTable
                 where x.UserId.Equals(userId)
                 select x.Username).First();
        }

        /// <summary>
        /// Looks up the userIUd for the user with the specified username.
        /// </summary>
        /// 
        /// <remarks>
        /// The method assumes that the user actually exists. Do not call this method without
        /// verifying that.
        /// </remarks>
        public static long GetUserIdForUsername(this TwitterDataSet data, string username)
        {
            createUserIfNecessary(data, username);

            return
                (from x in data.UserDataTable
                 where x.Username.Equals(username, StringComparison.OrdinalIgnoreCase)
                 select x.UserId).First();
        }

        /// <summary>
        /// Returns true iff the user represented by userId1 is following the user represented by 
        /// userId2.
        /// </summary>
        /// 
        /// <remarks>
        /// The method assumes that the users actually exist. Do not call this method without
        /// verifying that.
        /// 
        /// Also, for practical reasons, any user is treated as automatically following himself.
        /// </remarks>
        public static bool IsFollowing(this TwitterDataSet data, long userId1, long userId2)
        {
            return
                (userId1 == userId2) ||
                (from x in data.FollowingDataTable
                 where x.UserId.Equals(userId1) && x.FollowingUserId.Equals(userId2)
                 select x).Count() != 0;
        }

        /// <summary>
        /// Returns the timestamp at which the user represented by userId1 started following the
        /// user represented by userId2.
        /// </summary>
        /// 
        /// <remarks>
        /// The method assumes that the users actually exist and that the first user is actually
        /// following the second. Do not call this method without verifying that.
        /// </remarks>
        public static DateTime WhenStartedFollowing(this TwitterDataSet data, long userId1, long userId2)
        {
            return
                (from x in data.FollowingDataTable
                 where x.UserId.Equals(userId1) && x.FollowingUserId.Equals(userId2)
                 select x.WhenStarted).First();
        }

        /// <summary>
        /// Have the user specified by userId1 follow the user specified by userId2.
        /// </summary>
        /// 
        /// <remarks>
        /// The method assumes that the users actually exist and that the first user is not already
        /// following the second. Do not call this method without verifying that.
        /// </remarks>
        public static void Follow(this TwitterDataSet data, long userId1, long userId2)
        {
            TwitterDataSet.FollowingDataTableRow newRow = data.FollowingDataTable.NewFollowingDataTableRow();

            newRow.UserId = userId1;
            newRow.FollowingUserId = userId2;
            newRow.WhenStarted = DateTime.Now;

            data.FollowingDataTable.Rows.Add(newRow);
        }

        /// <summary>
        /// Have the user specified by userId1 post the specified message.
        /// </summary>
        /// 
        /// <remarks>
        /// The method assumes that the user actually exists.
        /// </remarks>
        public static void Post(this TwitterDataSet data, long userId, string message)
        {
            TwitterDataSet.TweetsDataTableRow row = data.TweetsDataTable.NewTweetsDataTableRow();

            row.UserId = userId;
            row.Timestamp = DateTime.Now;
            row.Message = message;

            data.TweetsDataTable.Rows.Add(row);
        }

        private static IEnumerable<TwitterDataSet.TweetsDataTableRow> getTweetsForUser(TwitterDataSet data, long userId)
        {
            return
                from x in data.TweetsDataTable
                where x.UserId.Equals(userId)
                orderby x.Timestamp ascending
                select x;
        }

        private static IEnumerable<TwitterDataSet.TweetsDataTableRow> getFollowedTweets(TwitterDataSet data, string username)
        {
            return getFollowedTweets(data, getFollowedUsers(data, username));
        }

        private static IEnumerable<TwitterDataSet.TweetsDataTableRow> combineTweets(
            IEnumerable<TwitterDataSet.TweetsDataTableRow> myTweets,
            IEnumerable<TwitterDataSet.TweetsDataTableRow> followedTweets)
        {
            return
                from x in myTweets.Concat(followedTweets)
                orderby x.Timestamp ascending
                select x;
        }

        private static IEnumerable<TwitterDataSet.TweetsDataTableRow> getAllTweetsForUsername(TwitterDataSet data, string username)
        {
            return
                from x in data.TweetsDataTable
                where x.UserId.Equals(GetUserIdForUsername(data, username))
                orderby x.Timestamp ascending                
                select x;
        }

        private static IEnumerable<TwitterDataSet.FollowingDataTableRow> getFollowedUsers(TwitterDataSet data, string username)
        {
            return
                from x in data.FollowingDataTable
                where x.UserId.Equals(GetUserIdForUsername(data, username))
                orderby x.WhenStarted ascending
                select x;
        }

        private static bool userExists(TwitterDataSet data, string username)
        {
            return
                (from x in data.UserDataTable
                 where x.Username.Equals(username, StringComparison.OrdinalIgnoreCase)
                 select x).Count() != 0;
        }

        /// <summary>
        /// Gets the tweets from ever user who corresponds to applicable rows in the Follow table.
        /// </summary>
        private static IEnumerable<TwitterDataSet.TweetsDataTableRow> getFollowedTweets(
            TwitterDataSet data,
            IEnumerable<TwitterDataSet.FollowingDataTableRow> follows)
        {
            // For each applicable row in the Follow table, agregate the corresponding tweets.
            return follows.Aggregate(
                (IEnumerable<TwitterDataSet.TweetsDataTableRow>)new List<TwitterDataSet.TweetsDataTableRow>(), 
                (current, followRow) => current.Concat(getFollowedTweets(data, followRow)));
        }

        /// <summary>
        /// Gets the tweets from the user who corresponds to the supplied row from the Follow table.
        /// </summary>
        private static IEnumerable<TwitterDataSet.TweetsDataTableRow> getFollowedTweets(
            TwitterDataSet data,
            TwitterDataSet.FollowingDataTableRow followRow)
        {
            long userId = followRow.FollowingUserId;
            DateTime dateStarted = followRow.WhenStarted;

            return 
                from x in data.TweetsDataTable
                where x.UserId.Equals(userId) && (x.Timestamp >= dateStarted)
                select x;
        }

        private static void createUserIfNecessary(TwitterDataSet data, string username)
        {
            if (!userExists(data, username))
            {
                TwitterDataSet.UserDataTableRow newRow = data.UserDataTable.NewUserDataTableRow();
                newRow.Username = username;
                data.UserDataTable.Rows.Add(newRow);
            }
        }
    }
}
