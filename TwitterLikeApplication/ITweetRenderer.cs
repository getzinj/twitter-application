﻿using System.Collections.Generic;

namespace com.jeffreygetzin.twitterExercise
{
    /// <summary>
    /// Represents a class that is capable of displaying tweets.
    /// </summary>
    interface ITweetRenderer
    {
        /// <summary>
        /// Display all the specified tweets (without showing the usernames of the persons who
        /// tweeted them).
        /// </summary>
        void displayTweets(IEnumerable<TwitterDataSet.TweetsDataTableRow> tweets);

        /// <summary>
        /// Display all the specified tweets, showing the usernames of the persons who tweeted 
        /// them.
        /// </summary>
        void displayTweetsWithUsernames(IEnumerable<TwitterDataSet.TweetsDataTableRow> tweets);
    }
}
