﻿using System.IO;

namespace com.jeffreygetzin.twitterExercise
{
    /// <summary>
    /// A Twitter-like service that allows posting and reading tweets.
    /// </summary>
    public class Twitter : ITwitter
    {
        private readonly TwitterDataSet data = new TwitterDataSet();
        private readonly ITweetRenderer renderer;

        public Twitter(TextWriter output)
        {
            renderer = new TweetRenderer(output);
        }

        public void Posting(string username, string message)
        {
            data.Post(data.GetUserIdForUsername(username), message);
        }

        public void Reading(string username)
        {
            renderer.displayTweets(data.ReadTweets(username));
        }

        public void Wall(string username)
        {
            renderer.displayTweetsWithUsernames(data.GetWallTweets(username));
        }

        public void Following(string username1, string username2)
        {
            long userId1 = data.GetUserIdForUsername(username1);
            long userId2 = data.GetUserIdForUsername(username2);
            
            if (!data.IsFollowing(userId1, userId2))
            {
                data.Follow(userId1, userId2);
            }
        }
    }
}
