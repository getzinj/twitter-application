﻿using System.Text.RegularExpressions;
using com.jeffreygetzin.twitterExercise;

namespace TwitterLikeApplication
{
    /// <summary>
    /// A parser for Twitter-like commands.
    /// </summary>
    /// 
    /// <remarks>
    /// I wrote this parser using regular-expressions since they were simpler and easier to write
    /// and debug. However, if I were to write a parser for a production system, I would likely use
    /// an LR(1) parser, probably generated from BNF with a parser generator such as YACC. Such a
    /// parser would provide better error handling, as we would be able to tell the user what 
    /// precisely he typed that was wrong, and what was expected by the parser.
    /// </remarks>
    public class TwitterCommandParser
    {
        private const string USERNAME1_MATCH_GROUP = "username1";
        private const string USERNAME2_MATCH_GROUP = "username2";
        private const string MESSAGE_GROUP_NAME = "message";

        /// <summary>
        /// username --> letter (letter | number | underscore)*
        /// </summary>
        private const string USERNAME_REGEX_PATTERN = @"[a-zA-Z][a-zA-Z0-9_]*";

        /// <summary>
        /// Zero or more whitespace characters.
        /// </summary>
        private const string OPTIONAL_WHITESPACE_PATTERN = @"[\s]*";

        /// <summary>
        /// One or more whitespace characters.
        /// </summary>
        private const string REQUIRED_WHITESPACE_PATTERN = @"[\s]+";

        private static readonly string USERNAME_ONLY_REGEX_PATTERN =
            @"__optionalwhitespace__(?<username1>__username__)__optionalwhitespace__$"
            .Replace("__username__", USERNAME_REGEX_PATTERN)
            .Replace("__optionalwhitespace__", OPTIONAL_WHITESPACE_PATTERN);

        private static readonly string MESSAGE_REGEX_PATTERN =
            @"__optionalwhitespace__(?<username1>__username__)__optionalwhitespace__(?<command>->)__optionalwhitespace__(?<message>.+)$"
            .Replace("__username__", USERNAME_REGEX_PATTERN)
            .Replace("__optionalwhitespace__", OPTIONAL_WHITESPACE_PATTERN);

        private static readonly string FOLLOWS_REGEX_PATTERN =
            @"__optionalwhitespace__(?<username1>__username__)__requiredwhitespace__(?<command>([Ff][Oo][Ll][Ll][Oo][Ww][Ss]){1})__requiredwhitespace__(?<username2>__username__)__optionalwhitespace__$"
            .Replace("__username__", USERNAME_REGEX_PATTERN)
            .Replace("__optionalwhitespace__", OPTIONAL_WHITESPACE_PATTERN)
            .Replace("__requiredwhitespace__", REQUIRED_WHITESPACE_PATTERN);

        private static readonly string WALL_REGEX_PATTERN =
            @"__optionalwhitespace__(?<username1>__username__)__requiredwhitespace__(?<command>[Ww][Aa][Ll][Ll])__optionalwhitespace__$"
            .Replace("__username__", USERNAME_REGEX_PATTERN)
            .Replace("__optionalwhitespace__", OPTIONAL_WHITESPACE_PATTERN)
            .Replace("__requiredwhitespace__", REQUIRED_WHITESPACE_PATTERN);

        /* 
         * I could have combined all of these into a single, monstrous regex. Yes, it would have 
         * resulted in better performance, but it would be a maintanence nightmare. Four regexes
         * (regexi?) are still O(4 * k * n) = O(n) for some constant k.
         */
        private static readonly Regex UsernameRegex = new Regex(USERNAME_ONLY_REGEX_PATTERN);
        private static readonly Regex MessageRegex = new Regex(MESSAGE_REGEX_PATTERN);
        private static readonly Regex FollowsRegex = new Regex(FOLLOWS_REGEX_PATTERN);
        private static readonly Regex WallRegex = new Regex(WALL_REGEX_PATTERN);

        public void parse(string input, out TwitterCommand command, out string username1, out string username2, out string message)
        {
            if (MessageRegex.IsMatch(input))
            {
                Match match = MessageRegex.Match(input);
                command = TwitterCommand.Posting;
                username1 = match.Groups[USERNAME1_MATCH_GROUP].ToString();
                username2 = null;
                message = match.Groups[MESSAGE_GROUP_NAME].ToString();
            }
            else if (FollowsRegex.IsMatch(input))
            {
                Match match = FollowsRegex.Match(input);
                command = TwitterCommand.Following;
                username1 = match.Groups[USERNAME1_MATCH_GROUP].ToString();
                username2 = match.Groups[USERNAME2_MATCH_GROUP].ToString();
                message = null;
            }
            else if (WallRegex.IsMatch(input))
            {
                Match match = WallRegex.Match(input);
                command = TwitterCommand.Wall;
                username1 = match.Groups[USERNAME1_MATCH_GROUP].ToString();
                username2 = null;
                message = null;
            }
            else if (UsernameRegex.IsMatch(input))
            {
                Match match = UsernameRegex.Match(input);
                command = TwitterCommand.Reading;
                username1 = match.Groups[USERNAME1_MATCH_GROUP].ToString();
                username2 = null;
                message = null;
            }
            else
            {
                command = TwitterCommand.Error;
                username1 = null;
                username2 = null;
                message = null;
            }
        }
    }
}
