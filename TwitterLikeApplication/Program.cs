﻿using System;
namespace com.jeffreygetzin.twitterExercise
{
    /// <summary>
    /// Twitter-like application designed to impress the snot out of the engineers at UBS Global 
    /// Operations IT Risk.
    /// </summary>
    internal static class Program
    {
        private static void Main(string[] args)
        {
            // Create an object that prompts for, and parses Twitter-like commands.
            IPrompt prompt = new Prompt(Console.Out, Console.In);

            // Create an object that executes Twitter-like commands.
            ITwitter twitter = new Twitter(Console.Out);

            while (true)
            {
                TwitterCommand command;
                string username1, username2, message;
                prompt.PromptForUserInput(out command, out username1, out username2, out message);

                switch (command)
                {
                    case TwitterCommand.Following:
                        twitter.Following(username1, username2);
                        break;
                    case TwitterCommand.Posting:
                        twitter.Posting(username1, message);
                        break;
                    case TwitterCommand.Reading:
                        twitter.Reading(username1);
                        break;
                    case TwitterCommand.Wall:
                        twitter.Wall(username1);
                        break;
                    default:
                        Console.Error.WriteLine("ERROR!");
                        break;
                }
            }
        }
    }
}



