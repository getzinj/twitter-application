﻿using System;

namespace com.jeffreygetzin.twitterExercise
{
    /// <summary>
    /// A collection of extension methods to display timestamps in a human-readable format.
    /// </summary>
    internal static class FormattingExtensionMethods
    {
        private static readonly TimeSpan ONE_MINUTE = new TimeSpan(0, 0, 1, 0);
        private static readonly TimeSpan ONE_HOUR   = new TimeSpan(0, 1, 0, 0);
        private static readonly TimeSpan ONE_DAY    = new TimeSpan(1, 0, 0, 0);

        /// <summary>
        /// Returns how long ago the timestamp was in a human-readable format.
        /// </summary>
        public static string Delta(this DateTime timestamp)
        {
            TimeSpan delta = DateTime.Now - timestamp;
            return format(delta) + " ago";
        }
        
        private static string format(TimeSpan delta)
        {
            return round(delta).ToPithyString();
        }

        private static TimeSpan round(TimeSpan timeSpan)
        {
            if (timeSpan < ONE_MINUTE)
            {
                return roundToNearestSecond(timeSpan); // Round anything under a minute to the nearest second.
            }
            else if (timeSpan < ONE_HOUR)
            {
                return roundToNearestMinute(timeSpan); // Round anything under an hour to the nearest minute.
            }
            else if (timeSpan < ONE_DAY)
            {
                return roundToNearestHour(timeSpan); // Round anything under a day to the nearest hour.
            }
            else
            {
                return roundToNearestDay(timeSpan); // Round anything longer to the nearest day.
            }
        }

        private static TimeSpan roundToNearestSecond(TimeSpan timeSpan)
        {
            int seconds = timeSpan.Seconds;
            int milliseconds = timeSpan.Milliseconds;

            int roundedSeconds = seconds + (int)Math.Round(milliseconds / 500f, 0);
            return new TimeSpan(0, 0, roundedSeconds);
        }

        private static TimeSpan roundToNearestMinute(TimeSpan timeSpan)
        {
            int minutes = timeSpan.Minutes;
            int seconds = timeSpan.Seconds;

            int roundedMinutes = minutes + (int) Math.Round(seconds/30f, 0);
            return new TimeSpan(0, roundedMinutes, 0);
        }

        private static TimeSpan roundToNearestHour(TimeSpan timeSpan)
        {
            int hours = timeSpan.Hours;
            int minutes = timeSpan.Minutes;

            int roundedHours = hours + (int)Math.Round(minutes / 30f, 0);
            return new TimeSpan(roundedHours, 0, 0);
        }

        private static TimeSpan roundToNearestDay(TimeSpan timeSpan)
        {
            int days = timeSpan.Days;
            int hours = timeSpan.Hours;

            int roundedDay = days + (int)Math.Round(hours / 12f, 0);
            return new TimeSpan(roundedDay, 0, 0, 0);
        }

        private static string ToPithyString(this TimeSpan timeSpan)
        {
            // Only one of days, minutes, hours, or seconds can be set.
            if (timeSpan.Days != 0)
            {
                return timeSpan.Days.ToString() + ' ' + (timeSpan.Days == 1 ? "day" : "days");
            }
            else if (timeSpan.Hours != 0)
            {
                return timeSpan.Hours.ToString() + ' ' + (timeSpan.Hours == 1 ? "hour" : "hours");
            }
            else if (timeSpan.Minutes != 0)
            {
                return timeSpan.Minutes.ToString() + ' ' + (timeSpan.Minutes == 1 ? "minute" : "minutes");
            }
            else
            {
                return timeSpan.Seconds.ToString() + ' ' + (timeSpan.Seconds == 1 ? "second" : "seconds");
            }
        }
    }
}
